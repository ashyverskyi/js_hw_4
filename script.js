// TASK 1

let num1;
let num2;

do {
    num1 = prompt("Enter first number:");

    if (num1 === null) {
        alert("You canceled the operation.");
        break;
    }

    num1 = parseFloat(num1);

    if (isNaN(num1)) {
        alert("Enter a valid number.");
    }
} while (isNaN(num1));

if (num1 !== null) {
    do {
        num2 = prompt("Enter second number:");

        if (num2 === null) {
            alert("You canceled the operation.");
            break;
        }

        num2 = parseFloat(num2);

        if (isNaN(num2)) {
            alert("Enter a valid number.");
        }
    } while (isNaN(num2));
}

if (!isNaN(num1) && !isNaN(num2)) {
    let start = Math.min(num1, num2);
    let end = Math.max(num1, num2);

    for (let i = Math.ceil(start); i <= end; i++) {
        console.log(i);
    }
}

// TASK 2

let userInput, number;

do {
    userInput = prompt("Enter a number");

    if (userInput === null) {
        alert("You canceled the operation.");
        break;
    }

    number = parseInt(userInput);


    if (isNaN(number) || number % 2 !== 0) {
        alert("Enter an even number.");
    }
} while (isNaN(number) || number % 2 !== 0);

if (number !== null) {
    console.log(`You have entered an even number: ${number}`);
}
